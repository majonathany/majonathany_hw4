/*
To indicate a dynamic cast that cannot be cast from source reference to target reference type, a bad cast exception is thrown.
*/


#include <iostream>

class A
{
public:
	virtual ~A()
	{
	}
};

class B : public A
{
};

using namespace std;

int main()
{
	A* a = new A;
	B& b = dynamic_cast<B&>(*a); //Bad cast exception
}